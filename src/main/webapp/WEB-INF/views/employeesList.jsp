<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Employee list </title>
</head>
<body>
<h3>Employee list of department: ${departmentTitle}</h3>
<table border="1">
    <thead>
    <tr>
        <th>ID</th>
        <th>First name</th>
        <th>Last name</th>
        <th>Email</th>
        <th>Date of birth</th>
        <th>Salary</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="employee" items="${employeeList}" >
        <tr>
            <td>${employee.employeeId}</td>
            <td>${employee.firstName}</td>
            <td>${employee.lastName}</td>
            <td>${employee.email}</td>
            <td>${employee.dob}</td>
            <td>${employee.salary}</td>
            <td>
                <a href="employee?action=editEmployee&employeeId=${employee.employeeId}"><span></span> Edit</a>
                <a href="employee?action=deleteEmployee&employeeId=${employee.employeeId}"><span></span> Delete</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<p><a href="employee?action=insertEmployee">Add employee</a></p>
<p><a href="index?action=departmentList">Back to departments</a></p>
</body>
</html>
