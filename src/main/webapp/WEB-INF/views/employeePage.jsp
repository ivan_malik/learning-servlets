<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add/Edit Employee</title>
</head>
<body>
<c:forEach var="error" items="${errors}" >
    <h4 style="color: brown">${error}</h4>
</c:forEach>
<table>
<form action="/employee" method="post" name="employeeForm" >
    <tr>
        <td>Employee id:</td>
        <td><input type="text" readonly="readonly" name="employeeId" value="${employee.employeeId}"/></td>
    </tr>
    <tr>
        <td>First name:</td>
        <td><input type="text" name="firstName" value="${employee.firstName}"/></td>
    </tr>
    <tr>
        <td>Last name:</td>
        <td><input type="text" name="lastName" value="${employee.lastName}"/></td>
    </tr>
    <tr>
        <td>Email:</td>
        <td><input type="email" name="email" value="${employee.email}"/></td>
    </tr>
    <tr>
        <td>Date of birth:</td>
        <td><input type="date" name="dob" required="required" value="${employee.dob}"/></td>
    </tr>
    <tr>
        <td>Salary:</td>
        <td><input type="text" required="required" name="salary" value="${employee.salary}"/></td>
    </tr>
    <tr>
        <td colspan="2" align="center">
            <input type="submit" value="Submit" />>
        </td>
    </tr>
</form>
</table>
</body>
</html>
