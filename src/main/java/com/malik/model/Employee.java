package com.malik.model;

import com.malik.validation.UniqueEmployeeEmail;
import net.sf.oval.constraint.*;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class Employee {

    private int employeeId;

    @Length(min = 3, max = 20, message = "First name`s length must be in range from 5 to 200 characters.")
    private String firstName;

    @Length(min = 3, max = 20, message = "Last name`s length must be in range from 5 to 200 characters.")
    private String lastName;

    @UniqueEmployeeEmail(message = "Unique email is required. Employee with such email is already exists.")
    @Email(message = "Email is not valid")
    private String email;

    @DateRange(format = "yyyyy.MM.dd", max = "2000.01.01", min = "1950.01.01", message = "Date of birth is not in the range 1950-01-01 through 2000-01-01")
    @NotNull
    private Date dob;

    @Range(max = 10000, min = 100, message = "Salary must be in range from 100 to 10 000.")
    private double salary;

    private int departmentId;

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastname) {
        this.lastName = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", dob=" + dob +
                ", salary=" + salary +
                ", departmentId=" + departmentId +
                '}';
    }
}
