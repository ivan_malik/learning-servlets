package com.malik.servlet;

import com.malik.dao.DepartmentDao;
import com.malik.dao.EmployeeDao;
import com.malik.model.Department;
import com.malik.model.Employee;
import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class EmployeeServlet extends HttpServlet {
    private EmployeeDao employeeDao;
    private DepartmentDao departmentDao;

    private final static Logger logger = Logger.getLogger(EmployeeServlet.class);

    public EmployeeServlet() {
        super();
        employeeDao = new EmployeeDao();
        departmentDao = new DepartmentDao();
    }

    private int depId = -1;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        String forward = "/WEB-INF/views/start.jsp";
        String action = request.getParameter("action");

        if(action.equalsIgnoreCase("showEmployees")) {
            forward = "/WEB-INF/views/employeesList.jsp";
            int departmentId = Integer.parseInt(request.getParameter("departmentId"));
            depId = departmentId;
            logger.info("Employees of department with ID" + depId + "have showed.");
            Department department = departmentDao.getDepartmentById(departmentId);
            request.setAttribute("departmentTitle", department.getTitle());
            request.setAttribute("employeeList", employeeDao.getAllEmployeesInDepartment(departmentId));
            logger.info("Show employees list");
        } else if(action.equalsIgnoreCase("editEmployee")) {
            forward = "/WEB-INF/views/employeePage.jsp";
            int employeeId = Integer.parseInt(request.getParameter("employeeId"));
            Employee employee = employeeDao.getEmployeeById(employeeId);
            logger.info("Get employee with ID: " + employeeId);
            request.setAttribute("employee", employee);
        } else if (action.equalsIgnoreCase("deleteEmployee")) {
            int employeeId = Integer.parseInt(request.getParameter("employeeId"));
            employeeDao.deleteEmployee(employeeId);
            System.out.println(depId);
            request.setAttribute("employeeList", employeeDao.getAllEmployeesInDepartment(depId));
            forward = "/WEB-INF/views/employeesList.jsp";

        } else if(action.equalsIgnoreCase("insertEmployee")) {
            forward = "/WEB-INF/views/employeePage.jsp";
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher(forward);
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");

        Employee employee = new Employee();
        employee.setFirstName(request.getParameter("firstName"));
        employee.setLastName(request.getParameter("lastName"));
        employee.setEmail(request.getParameter("email"));
        employee.setDob(Date.valueOf(request.getParameter("dob")));
        employee.setSalary(Double.parseDouble(request.getParameter("salary")));
        employee.setDepartmentId(depId);
        String employeeId = request.getParameter("employeeId");

        if(!(employeeId == null || employeeId.isEmpty()))
            employee.setEmployeeId(Integer.parseInt(employeeId));

        Validator validator = new Validator();
        List<ConstraintViolation> violations = validator.validate(employee);
        if (violations.size() > 0) {
            logger.warn(employee + " is not valid.");
            List<String> errors = new ArrayList<>();
            for (ConstraintViolation cv : violations) {
                errors.add(cv.getMessage());
            }

            request.setAttribute("errors", errors);
            request.setAttribute("employee", employee);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/employeePage.jsp");
            requestDispatcher.forward(request, response);

        } else {
            if (employeeId == null || employeeId.isEmpty() || Integer.parseInt(employeeId) == 0) {
                employeeDao.addEmployee(employee);
            } else {
                employee.setEmployeeId(Integer.parseInt(employeeId));
                employeeDao.updateEmployee(employee);
            }
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/employeesList.jsp");
            request.setAttribute("employeeList", employeeDao.getAllEmployeesInDepartment(depId));
            requestDispatcher.forward(request, response);
        }
    }
}
