package com.malik.validation;

import com.malik.dao.EmployeeDao;
import com.malik.model.Employee;
import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.constraint.NotEqual;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;

import java.util.List;

public class UniqueEmailCheck extends AbstractAnnotationCheck<NotEqual> {

    @Override
    public boolean isSatisfied(Object validatedObject, Object valueToValidate, OValContext context, Validator validator) throws OValException {
        EmployeeDao employeeDao = new EmployeeDao();
        List<Employee> employees = employeeDao.getAllEmployees();
        if (valueToValidate == null)
            return false;

        for (Employee employee : employees ) {
            if (employee.getEmployeeId() == ((Employee)validatedObject).getEmployeeId())
                continue;
            if (employee.getEmail().equals(valueToValidate))
                return false;
        }

        return true;
    }
}
