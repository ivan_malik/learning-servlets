package com.malik.dao;

import com.malik.model.Department;
import com.malik.util.DBUtil;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentDao {

    private Connection connection;
    private final static Logger logger = Logger.getLogger(DepartmentDao.class);

    public DepartmentDao() {
        connection = DBUtil.getConnection();
    }

    public void addDepartment(Department department) {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO department (title, description) VALUES ( ?, ?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, department.getTitle());
            preparedStatement.setString(2, department.getDescription());
            preparedStatement.executeUpdate();
            logger.info("Added department: " + department);
        } catch (SQLException e) {
            logger.error("Problem with adding a department." + e);
        } finally {
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {}
        }
    }

    public void deleteDepartment(int departmentId) {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "DELETE FROM department WHERE department_id = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, departmentId);
            preparedStatement.executeUpdate();
            logger.info("Deleted department with ID: " + departmentId);
        } catch (SQLException e) {
            logger.error("Problem with deleting a department." + e);
        }finally {
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {}
        }
    }

    public void updateDepartment(Department department) {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE department SET title=?, description=? WHERE department_id=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, department.getTitle());
            preparedStatement.setString(2, department.getDescription());
            preparedStatement.setInt(3, department.getDepartmentId());
            preparedStatement.executeUpdate();
            logger.info("Department updated: " + department);
        } catch (SQLException e) {
            logger.error("Problem with updating a department." + e);
        } finally {
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {}
        }
    }

    public Department getDepartmentById(int departmentId) {
        Department department = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT * FROM department WHERE department_id = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, departmentId);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                department = new Department();
                department.setDepartmentId(resultSet.getInt("department_id"));
                department.setTitle(resultSet.getString("title"));
                department.setDescription(resultSet.getString("description"));
            }
        } catch (SQLException e ) {
            logger.error("Problem with getting department by id." + e);
        } finally {
            try { if (resultSet != null) resultSet.close(); } catch (Exception e) {}
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {}
        }

        return department;
    }

    public List<Department> getAllDepartments() {
        List<Department> departmentList = new ArrayList<>();
        ResultSet resultSet = null;
        Statement statement = null;
        try {
            String sql = "SELECT * FROM department";
            statement = connection.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Department department = new Department();
                department.setDepartmentId(resultSet.getInt("department_id"));
                department.setTitle(resultSet.getString("title"));
                department.setDescription(resultSet.getString("description"));
                departmentList.add(department);
            }
        } catch (SQLException e) {
            logger.error("Problem with getting department list." + e);
        }finally {
            try { if (resultSet != null) resultSet.close(); } catch (Exception e) {}
            try { if (statement != null) statement.close(); } catch (Exception e) {}
        }

        return departmentList;
    }
}
