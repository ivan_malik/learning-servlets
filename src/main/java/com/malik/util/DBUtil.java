package com.malik.util;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {
    private static Connection connection = null;
    private final static Logger logger = Logger.getLogger(DBUtil.class);

    public static Connection getConnection() {
        if (connection != null) {
            return connection;
        }
        else {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://localhost:3306/test_project";
                String user = "root";
                String password = "pomidor000";
                connection = DriverManager.getConnection(url, user, password);
            } catch (ClassNotFoundException e) {
                logger.error("Problem with loading driver" + e);
            } catch (SQLException e) {
                logger.error("SQLException had occurred when connection was establishing." + e);
            }
            logger.info("Connection successful established.");
            return connection;
        }
    }
}
