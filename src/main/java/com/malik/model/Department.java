package com.malik.model;

import com.malik.validation.UniqueDepartmentTitle;
import net.sf.oval.constraint.Length;
import net.sf.oval.constraint.NotNull;

public class Department {

    private int departmentId;

    @UniqueDepartmentTitle(message = "Unique title is required. Department with such title is already exists.")
    @Length(min = 5, max = 30, message = "Title`s length must be in range from 5 to 30 characters.")
    private String title;

    @Length(min = 5, max = 200, message = "Description`s length must be in range from 5 to 200 characters.")
    private String description;

    public Department() {
    }

    public Department(int departmentId, String title, String description) {
        this.departmentId = departmentId;
        this.title = title;
        this.description = description;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentId=" + departmentId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
