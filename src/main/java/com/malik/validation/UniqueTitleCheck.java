package com.malik.validation;

import com.malik.dao.DepartmentDao;
import com.malik.model.Department;
import net.sf.oval.Validator;
import net.sf.oval.configuration.annotation.AbstractAnnotationCheck;
import net.sf.oval.constraint.NotEqual;
import net.sf.oval.context.OValContext;
import net.sf.oval.exception.OValException;

import java.util.List;

public class UniqueTitleCheck extends AbstractAnnotationCheck<NotEqual> {

    @Override
    public boolean isSatisfied(Object validatedObject, Object valueToValidate, OValContext context, Validator validator) throws OValException {
        DepartmentDao departmentDao = new DepartmentDao();
        List<Department> departments = departmentDao.getAllDepartments();
        if (valueToValidate == null)
            return false;

        for (Department department : departments) {
            if (department.getDepartmentId() == ((Department) validatedObject).getDepartmentId() )
                continue;
            if ((department.getTitle().equals(valueToValidate)) )
                return false;
        }

        return true;
    }
}
