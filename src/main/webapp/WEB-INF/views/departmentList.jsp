<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Department list</title>
</head>
<body>
<h3>Department`s list</h3>
<table border="1">
    <thead>
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Description</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="department" items="${list}" >
        <tr>
            <td>${department.departmentId}</td>
            <td>${department.title}</td>
            <td>${department.description}</td>
            <td>
                <a href="index?action=editDepartment&departmentId=${department.departmentId}"><span></span> Edit</a>
                <a href="index?action=deleteDepartment&departmentId=${department.departmentId}"><span></span> Delete</a>
                <a href="employee?action=showEmployees&departmentId=${department.departmentId}"><span></span> Show employees</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<p><a href="index?action=insertDepartment">Add department</a></p>
</body>
</html>
