package com.malik.servlet;

import com.malik.dao.DepartmentDao;
import com.malik.model.Department;
import net.sf.oval.ConstraintViolation;
import net.sf.oval.Validator;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class StartServlet extends HttpServlet {
    private DepartmentDao departmentDao;

    private final static Logger logger = Logger.getLogger(StartServlet.class);

    public StartServlet() {
        super();
        departmentDao = new DepartmentDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward = "/WEB-INF/views/start.jsp";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("deleteDepartment")){
            int departmentId = Integer.parseInt(request.getParameter("departmentId"));
            departmentDao.deleteDepartment(departmentId);
            forward = "/WEB-INF/views/departmentList.jsp";
            request.setAttribute("list", departmentDao.getAllDepartments());
        } else if (action.equalsIgnoreCase("editDepartment")){
            forward = "/WEB-INF/views/departmentPage.jsp";
            int departmentId = Integer.parseInt(request.getParameter("departmentId"));
            Department department = departmentDao.getDepartmentById(departmentId);
            logger.info("Get department with ID " + departmentId);
            request.setAttribute("department", department);
        } else if (action.equalsIgnoreCase("departmentList")) {
            forward = "/WEB-INF/views/departmentList.jsp";
            request.setAttribute("list", departmentDao.getAllDepartments());
            logger.info("Show department list");
        }
        else if (action.equalsIgnoreCase("insertDepartment")){
            forward = "/WEB-INF/views/departmentPage.jsp";
        }

        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher(forward);
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Department department = new Department();
        department.setTitle(request.getParameter("title"));
        department.setDescription(request.getParameter("description"));
        String departmentId = request.getParameter("departmentId");
        if(!(departmentId == null || departmentId.isEmpty()))
            department.setDepartmentId(Integer.parseInt(departmentId));

        Validator validator = new Validator();
        List<ConstraintViolation> violations = validator.validate(department);
        if (violations.size() > 0) {
            logger.warn(department + " is not valid.");
            List<String> errors = new ArrayList<>();
            for (ConstraintViolation cv : violations) {
                errors.add(cv.getMessage());
            }

            request.setAttribute("errors", errors);
            request.setAttribute("department", department);
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/WEB-INF/views/departmentPage.jsp");
            requestDispatcher.forward(request, response);

        } else {
            if (departmentId == null || departmentId.isEmpty() || Integer.parseInt(departmentId) == 0) {
                departmentDao.addDepartment(department);
            } else {
                department.setDepartmentId(Integer.parseInt(departmentId));
                departmentDao.updateDepartment(department);
            }
            RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/views/departmentList.jsp");
            request.setAttribute("list", departmentDao.getAllDepartments());
            view.forward(request, response);
        }
    }
}
