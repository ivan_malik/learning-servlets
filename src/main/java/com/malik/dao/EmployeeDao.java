package com.malik.dao;

import com.malik.model.Employee;
import com.malik.util.DBUtil;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDao {

    private Connection connection;
    private final static Logger logger = Logger.getLogger(EmployeeDao.class);

    public EmployeeDao() {
        connection = DBUtil.getConnection();
    }

    public void addEmployee(Employee employee) {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "INSERT INTO employee (first_name, last_name, email, dob, salary, department_id)" +
                    "VALUES (?, ?, ?, ?, ?, ?);";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, employee.getFirstName());
            preparedStatement.setString(2, employee.getLastName());
            preparedStatement.setString(3, employee.getEmail());
            preparedStatement.setDate(4, employee.getDob());
            preparedStatement.setDouble(5, employee.getSalary());
            preparedStatement.setInt(6, employee.getDepartmentId());
            preparedStatement.executeUpdate();
            logger.info("Added employee: " + employee);
        } catch (SQLException e) {
            logger.error("Problem with adding an employee" + e);
        } finally {
            try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException e) {}
        }
    }

    public void deleteEmployee(int employeeId) {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "DELETE FROM employee where employee_id=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, employeeId);
            preparedStatement.executeUpdate();
            logger.info("Deleted employee with ID : " + employeeId);
        } catch (SQLException e) {
            logger.error("Problem with deleting an employee." + e);
        } finally {
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {}
        }
    }

    public void updateEmployee(Employee employee) {
        PreparedStatement preparedStatement = null;
        try {
            String sql = "UPDATE employee SET first_name=?, last_name=?, email=?, dob=?, salary=?, department_id=? WHERE employee_id=?;";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, employee.getFirstName());
            preparedStatement.setString(2, employee.getLastName());
            preparedStatement.setString(3, employee.getEmail());
            preparedStatement.setDate(4, employee.getDob());
            preparedStatement.setDouble(5, employee.getSalary());
            preparedStatement.setInt(6, employee.getDepartmentId());
            preparedStatement.setInt(7, employee.getEmployeeId());
            preparedStatement.executeUpdate();
            logger.info("Updated an employee: " + employee);
        } catch (SQLException e) {
            logger.error("Problem with updating an employee." + e);
        } finally {
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {}
        }
    }

    public Employee getEmployeeById(int employeeId) {
        Employee employee = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT * FROM employee WHERE employee_id=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, employeeId);
            resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                employee = new Employee();
                employee.setEmployeeId(resultSet.getInt("employee_id"));
                employee.setFirstName(resultSet.getString("first_name"));
                employee.setLastName(resultSet.getString("last_name"));
                employee.setEmail(resultSet.getString("email"));
                employee.setDob(resultSet.getDate("dob"));
                employee.setSalary(resultSet.getDouble("salary"));
                employee.setDepartmentId(resultSet.getInt("department_id"));
            }
        } catch (SQLException e) {
            logger.error("Problem with getting employee by id" + e);
        } finally {
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {}
            try { if (resultSet != null) resultSet.close(); } catch (Exception e) {}
        }

        return employee;
    }

    public List<Employee> getAllEmployeesInDepartment(int departmentId) {
        List<Employee> list = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT * FROM employee WHERE department_id=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, departmentId);
            resultSet = preparedStatement.executeQuery();

            enumerateEmployees(list, resultSet);
        } catch (SQLException e) {
            logger.error("Problem with getting employee list in department" + e);
        } finally {
            try { if (resultSet != null) resultSet.close(); } catch (Exception e) {}
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {}
        }

        return list;
    }

    public List<Employee> getAllEmployees() {
        List<Employee> list = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            String sql = "SELECT * FROM employee";
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            enumerateEmployees(list, resultSet);
        } catch (SQLException e) {
            logger.error("Problem with getting all employees" + e);
        } finally {
            try { if (resultSet != null) resultSet.close(); } catch (Exception e) {}
            try { if (preparedStatement != null) preparedStatement.close(); } catch (Exception e) {}
        }

        return list;
    }

    private void enumerateEmployees(List<Employee> list, ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            Employee employee = new Employee();
            employee.setEmployeeId(resultSet.getInt("employee_id"));
            employee.setFirstName(resultSet.getString("first_name"));
            employee.setLastName(resultSet.getString("last_name"));
            employee.setEmail(resultSet.getString("email"));
            employee.setDob(resultSet.getDate("dob"));
            employee.setSalary(resultSet.getDouble("salary"));
            employee.setDepartmentId(resultSet.getInt("department_id"));
            list.add(employee);
        }
    }
}
