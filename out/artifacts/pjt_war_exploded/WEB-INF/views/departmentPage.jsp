<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add/ Edit department</title>
</head>
<body>
<h1>New/Edit department</h1>
<c:forEach var="error" items="${errors}" >
    <h4 style="color: brown">${error}</h4>
</c:forEach>
<div>
<table>
    <form action="/index" method="post" name="departmentForm" >
        <tr>
            <td>Department id:</td>
            <td><input type="text" readonly="readonly" name="departmentId" value="${department.departmentId}"/></td>
        </tr>
        <tr>
            <td>Title:</td>
            <td><input type="text" name="title" value="${department.title}"/></td>
        </tr>
        <tr>
            <td>Description:</td>
            <td><input type="text" name="description" value="${department.description}"/></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <input type="submit" value="Submit" />
            </td>
        </tr>
    </form>
</table>
</div>
</body>
</html>
